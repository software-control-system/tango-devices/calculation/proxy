#ifndef _PROXY_TASK_H_
#define _PROXY_TASK_H_

#include <boost/smart_ptr.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <yat/threading/Task.h>
#include <tango.h>
#include "ThreadExiter.h"
#include "Cache.h"
#include "ProxyInterface.h"
#include "Converter.h"

namespace Proxy_ns
{
  using boost::posix_time::ptime;
  using boost::posix_time::time_duration;

  class Timer
  {
  public:
    Timer();

    void restart();

    time_duration elapsed( void );
    double elapsed_sec ();
    double elapsed_msec ();
    std::string str_elapsed_ms();

  private:

    static ptime now( void );

    ptime start_time_;
  };


  struct Properties
  {
    Properties( Tango::Device_3Impl* device );

    std::vector<std::string> attr_name_list;
    std::vector<std::string> command_name_list;
    std::string              device_proxy_name;
    long                     device_proxy_timeout;
    bool                     update_properties;
    long                     refresh_rate_ms;
    long                     max_consecutive_read_failures;
  };

  	struct InitStatus
    {
	  bool fail;
	  bool success;	  
      std::string status;
    };
	
  class ProxyTask : public yat::Task,
                    public Tango::LogAdapter
  {
  public:

    ProxyTask( Tango::Device_3Impl* device );

    ~ProxyTask();
	

	InitStatus getInitializationStatus();
	
  public:

    void read_attr_hardware();

    std::pair< Tango::DevState, std::string > get_state_status();

  protected:

    virtual void handle_message (yat::Message& msg)
      throw (yat::Exception);

  private:
    void init();
    void cleanup();

    bool init_interface();

    void update_read_values();
    void set_invalid_read_values();
	
	void getFallback();
	bool connexionChecked_;
	
	short targetProxyInterface();
	
	

    struct WriteValue
    {
      std::string attr_name;
      std::string distant_attr_name;
      boost::any data;
    };
	


    // called form client
    void write_value(const std::string& attr_name, const std::string& distant_attr_name, boost::any data);
    
    // called internally in the thread
    void on_write_value( WriteValue& w_val );

    Tango::Device_3Impl* device_;
    Properties properties_;
    DeviceProxyP dev_proxy_;
    //Tango::DeviceProxy* internal_dev_proxy_;
   	
	
    struct AttrDesc
    {
      ProxyAttrP attr;
      Tango::AttributeInfo distant_attr_info;
      ConverterP converter;
    };

    std::vector< AttrDesc > attributes_list_;
    std::vector< std::string > attributes_to_read_;

    std::vector< ProxyCmdP > commands_list_;

    CacheP cache_;
    Timer last_valid_update_time_;
    int update_failures_;
    Tango::DevFailed last_error_;

    yat::Mutex state_status_mutex_;
    yat::Mutex init_mutex_;
    Tango::DevState state_;
    std::string     status_;
	
	InitStatus initStatus_;
	
	int retryNr_ ;

  };

  typedef boost::shared_ptr< ProxyTask > ProxyTaskP;


}

#endif
