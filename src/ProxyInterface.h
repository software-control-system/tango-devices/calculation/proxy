#ifndef _PROXY_ATTR_FACTORY_H_
#define _PROXY_ATTR_FACTORY_H_

#include <boost/noncopyable.hpp>
#include <yat/utils/Singleton.h>
#include <tango.h>
#include "Cache.h"
#include <yat/utils/Logging.h>
namespace Proxy_ns
{
  typedef boost::shared_ptr< Tango::DeviceProxy > DeviceProxyP;

  struct ProxyAttr
  {
    virtual ~ProxyAttr();

    virtual Tango::Attr* get_tango_interface() = 0;

    virtual void on_read_attr_hardware() = 0;
  };

  struct ProxyCmd
  {
    virtual ~ProxyCmd();

    virtual Tango::Command* get_tango_interface() = 0;
  };


  typedef boost::shared_ptr< ProxyAttr > ProxyAttrP;
  typedef boost::shared_ptr< ProxyCmd >  ProxyCmdP;
	
  class ProxyInterfaceFactory : public yat::Singleton<ProxyInterfaceFactory>,
								private boost::noncopyable  
  {
  public:
    ProxyAttrP create_attr( const std::string& attr_name,
                            const Tango::AttributeInfo& attr_info,
                            CacheP cache );

    ProxyCmdP create_cmd  ( const std::string& cmd_name,
                            const Tango::CommandInfo& cmd_info,
                            DeviceProxyP proxy , 
							Tango::Device_3Impl* device	);

  };

}

#endif
