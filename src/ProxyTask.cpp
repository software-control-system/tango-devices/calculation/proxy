#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/bind.hpp>
#include <functional>
#include <algorithm>
#include <sstream>
#include "ProxyTask.h"
#include "Proxy.h"
#include "Exception.h"
#include <yat/utils/Logging.h>
int RETRIES_BEFORE_OPEN_CIRCUIT_BREAKER = 20;
static const size_t kDEFAUT_CMD_TMO = 3000;
int WAIT_CIRCUIT_BREAKER_CLOSE = 3000;//3s between each retry
int WAIT_CIRCUIT_BREAKER_OPEN = 30000;//30s after 20 retry
int Scalar_Data_Format = 3;
short distInterfaceNotInitializedYet = 0;
short distInterfaceInitialized = 1;
short attributeNotFindErr = 2;
std::string LOG_ATTR_NAME = "log";
std::string ATTR_PROXY_INTERFACE_STATUS_NAME = "interface_ready";


namespace Proxy_ns
{
  // compares two given string for case-insensitive equality 
  struct equals_nocase : public std::binary_function< const std::string&, const std::string&, bool>
  {
    bool operator ()( const std::string& compared1, const std::string& compared2 )
    {
      return ( compared1.size() != compared2.size() )
              ? false
              : std::equal( compared1.begin(),
                            compared1.end(),
                            compared2.begin(),
                            boost::algorithm::is_iequal() );
    }
  };

  // erases the elements of a container with a predicate
  template<class Container, class Pred>
  void erase_if(Container & c, Pred pred)
  {
      c.erase(std::remove_if(c.begin(), c.end(), pred), c.end());
  }


  Timer::Timer()
  {
    restart();
  }

  void Timer::restart()
  {
    start_time_ = now();
  }

  time_duration Timer::elapsed( void )
  {
    return now() - start_time_;
  }

  double Timer::elapsed_sec ()
  {
    return 1E-3 * elapsed_msec();
  }

  double Timer::elapsed_msec ()
  {
#pragma warning(push)
#pragma warning(disable : 4244)
    return elapsed().total_milliseconds();
#pragma warning(pop)
  }

  std::string Timer::str_elapsed_ms()
  {
    time_duration e = elapsed();
    e = boost::posix_time::milliseconds(e.total_milliseconds()); // remove microseconds
    return boost::erase_tail_copy( to_simple_string(e), 3 );
  }

  ptime Timer::now( void )
  {
    return boost::posix_time::microsec_clock::local_time();
  }

  /******************************************************
  *
  *   Properties CTOR
  *
  *******************************************************/
  
  Properties::Properties( Tango::Device_3Impl* device )
    : attr_name_list(1, "*"),
      command_name_list(1, "*"),
      device_proxy_name("the/device/name"),
      device_proxy_timeout(3000),
      update_properties(false),
      refresh_rate_ms(1000),
      max_consecutive_read_failures(1)
  {

    // get properties from DB
    Tango::DbData	dev_prop;
    dev_prop.push_back(Tango::DbDatum("AttributeNameList"));
    dev_prop.push_back(Tango::DbDatum("CommandNameList"));
    dev_prop.push_back(Tango::DbDatum("DeviceProxy"));
    dev_prop.push_back(Tango::DbDatum("DeviceProxyTimeout"));
    dev_prop.push_back(Tango::DbDatum("UpdateProperties"));
    dev_prop.push_back(Tango::DbDatum("RefreshRateMs"));
    dev_prop.push_back(Tango::DbDatum("MaxConsecutiveReadFailures"));
    dev_prop.push_back(Tango::DbDatum("CleanUpDeviceAttributesAtShutdown"));

    device->get_db_device()->get_property(dev_prop);

    // extract them if not empty, otherwise put default value
    if ( !dev_prop[0].is_empty() )
      dev_prop[0] >> attr_name_list;
    else
      dev_prop[0] << attr_name_list;

    if ( !dev_prop[1].is_empty() )
      dev_prop[1] >> command_name_list;
    else
      dev_prop[1] << command_name_list;

    if ( !dev_prop[2].is_empty() )
      dev_prop[2] >> device_proxy_name;
    else
      dev_prop[2] << device_proxy_name;

    if ( !dev_prop[3].is_empty() )
      dev_prop[3] >> device_proxy_timeout;
    else
      dev_prop[3] << device_proxy_timeout;

    if ( !dev_prop[4].is_empty() )
      dev_prop[4] >> update_properties;
    else
      dev_prop[4] << update_properties;

    if ( !dev_prop[5].is_empty() )
      dev_prop[5] >> refresh_rate_ms;
    else
      dev_prop[5] << refresh_rate_ms;

    if ( !dev_prop[6].is_empty() )
      dev_prop[6] >> max_consecutive_read_failures;
    else
      dev_prop[6] << max_consecutive_read_failures;

 //   device->get_db_device()->put_property( dev_prop ); JIRA TANGODEVIC-1245
  }

/******************************************************
  *
  *   ProxyTask CTOR
  *
  *******************************************************/
  const size_t MSGID_WRITE_REQUEST = yat::FIRST_USER_MSG + 1001;
  const size_t INIT_PROXY_CONNEXION = yat::FIRST_USER_MSG + 1002;
  
  ProxyTask::ProxyTask( Tango::Device_3Impl* device )
    : yat::Task(),
      Tango::LogAdapter(device),
      device_(device),
      properties_(device),
      update_failures_(0),
      state_( Tango::INIT ),
      status_( "Initialization..." )
  {
	this->connexionChecked_ = false;
	this->initStatus_.fail = false;
	this->initStatus_.success = false;
	this->initStatus_.status = "";
	this->retryNr_ = 0;
	attributes_list_.clear();

	try {
      this->init();
    }
    catch( ... ){
      this->cleanup();
      throw;
    }

  }
/******************************************************
  *
  *   ProxyTask DTOR
  *
  *******************************************************/
  ProxyTask::~ProxyTask()
  {
	 DEBUG_STREAM << " ProxyTask::~ProxyTask()" << ENDLOG;
	this->cleanup();
  }
/******************************************************
  *
  *   ProxyTask init
  *
  *******************************************************/
  void ProxyTask::init()
  {
    DEBUG_STREAM << " void ProxyTask::init()" << ENDLOG;
    WriteFunction w_func = boost::bind(&ProxyTask::write_value, this, _1, _2, _3);
     cache_.reset( new Cache( w_func ) );
 
	this->enable_periodic_msg( false );
	this->go(properties_.device_proxy_timeout);
	//rest of the set up is in INIT_PROXY_CONNEXION (TANGODEVIC-1465)
  }
  /******************************************************
  *
  *   ProxyTask initializationStatus
  *
  *******************************************************/
InitStatus ProxyTask::getInitializationStatus()
{
  DEBUG_STREAM << "InitStatus ProxyTask::getInitializationStatus()" << ENDLOG;
	return this->initStatus_;
}
  
  /******************************************************
  *
  *   ProxyTask cleanup
  *
  *******************************************************/
  void ProxyTask::cleanup()
  {
    DEBUG_STREAM << "void ProxyTask::cleanup()" << ENDLOG;
    // thread has now been joined
    // we are in the thread of the client
    // remove the attributes here
    // do NOT free them, there are deallocated automagically
    yat::MutexLock guard(init_mutex_);

	int i = 0;
    BOOST_FOREACH( AttrDesc& desc, attributes_list_ )
    {
		device_->remove_attribute( desc.attr->get_tango_interface(), false, false );
		i++;
    }
	
	attributes_list_.clear();
    attributes_to_read_.clear();
    device_->get_device_class()->get_command_list().resize(3);
    device_ = 0;
  }
/******************************************************
  *
  *   ProxyTask read_attr_hardware
  *
  *******************************************************/

  void ProxyTask::read_attr_hardware()
  {
     DEBUG_STREAM << "void ProxyTask::read_attr_hardware()" << ENDLOG;
    BOOST_FOREACH( AttrDesc& desc, attributes_list_ )
    {
      desc.attr->on_read_attr_hardware();
    }
    
  }
/******************************************************
  *
  *   ProxyTask handle_message
  *
  *******************************************************/
  void ProxyTask::handle_message (yat::Message& msg)
    throw (yat::Exception)
  {
      switch ( msg.type() )
      {
      case yat::TASK_INIT:
        {
			DEBUG_STREAM << "TASK_INIT" << ENDLOG;
      this->connexionChecked_ = false;
      this->initStatus_.fail = false;
      this->initStatus_.success = false;
			try {
			//	Try to initialize the proxy first
				dev_proxy_.reset( new Tango::DeviceProxy(properties_.device_proxy_name) );
				this->post(INIT_PROXY_CONNEXION);
			}
			catch( Tango::DevFailed& df ){
				this->initStatus_.fail = true;
				this->initStatus_.success = false;
				this->initStatus_.status ="Task initialisation failed : \nCouldn't create proxy \nException while trying to create proxy object \nCheck DeviceProxy property \n ";
			}
          
        }
        break;
	   
      case yat::TASK_EXIT:
        {
			DEBUG_STREAM << "TASK_EXIT" << ENDLOG;
        }
        break;
        
      case yat::TASK_PERIODIC:
        {
         // DEBUG_STREAM << "TASK_PERIODIC" << ENDLOG;
          try{
            Timer timer;
            this->update_read_values();
            update_failures_ = 0;
            last_valid_update_time_.restart();
            // INFO_STREAM << "PERIODIC done in " << timer.elapsed_msec() << " ms" << ENDLOG;
          }
          catch( Tango::DevFailed& df ){
            ERROR_STREAM << df << ENDLOG;
            // don't throw here because we are in a PERIODIC msg handler
            last_error_ = df;
            update_failures_++;
            if (update_failures_ > properties_.max_consecutive_read_failures)
            {
              this->set_invalid_read_values();
            }
          }
        }
        break;
	  //FalilouT. TANGODEVIC-1465//////////////////////////////////////////////////
	  case INIT_PROXY_CONNEXION:
	  {
		DEBUG_STREAM << "INIT_PROXY_CONNEXION try to ping proxy..." << ENDLOG;
			  try {
					//Is proxy Reachable?, If not then retry using getFallback until it's ok
					if (dev_proxy_){
						dev_proxy_->ping();
					}
        } catch (...) {
          //Didn't work wait and try again
					this->getFallback();
          this-> status_ = "Initialization running, PING not responding ... \nWaiting for target device : \n"+ properties_.device_proxy_name + " to respond...";
					this->post(INIT_PROXY_CONNEXION);
          break;
				}
        try{
          if (this->init_interface()){
            this->enable_periodic_msg (true);
            this->set_periodic_msg_period( properties_.refresh_rate_ms );
            this->initStatus_.success = true;
          }
          else{
            //Distant interface wasn't ready, try again
            this->getFallback();
            this-> status_ = "Initialization running...\nWaiting for target device : \n"+ properties_.device_proxy_name + " to be initialized. \nTANGODEVIC-765 :\nMake sure that distant device's name is different than current Proxy!";
            this->post(INIT_PROXY_CONNEXION);
            break;
          }
          
        }catch( ...){
          this->initStatus_.fail = true;
          this->initStatus_.success = false;
        }
				
	  }
	  break;
      case MSGID_WRITE_REQUEST:
   		{
			try
			{
				// first get the state of the read_only attribute
				//
				Proxy_ns::Proxy * p = 0;
				try 
				{
					p = reinterpret_cast<Proxy_ns::Proxy*> (device_);
				}
				catch (...)
				{
					throw_devfailed("EXECUTION_ERROR",
						"reinterpret_cast<ProxyTask::handle_message>  failed",
						"ProxyTask::handle_message:MSGID_WRITE_REQUEST");
				}
// Now check that the read_only attribute allows attributes to be written
				if (p->read_only())	
					throw_devfailed("EXECUTION_ERROR",
					"Writing attribute is not allowed because Proxy is read_only",
					"Command::execute");
						if (p->read_only_2())	
					throw_devfailed("EXECUTION_ERROR",
					"Writing attribute is not allowed because Proxy is read_only_2",
					"Command::execute");

						WriteValue* w_val = 0;
						msg.detach_data( w_val );
						boost::scoped_ptr< WriteValue > w_val_guard( w_val );

						DEBUG_STREAM << "Writing " << w_val->distant_attr_name << " and updating data..." << ENDLOG;
						Timer timer;
						this->on_write_value( *w_val );
						this->update_read_values();
						DEBUG_STREAM << "Done in " << timer.elapsed_sec() << " s" << ENDLOG;
				}
				catch( Tango::DevFailed& df )
				{
					ERROR_STREAM << df << ENDLOG;
					throw_yat_exception( df );
				}
			}
			break;
		}
  }
//FalilouT. TANGODEVIC-1465//////////////////////////////////////////////////
void ProxyTask::getFallback() 
{
  DEBUG_STREAM << "void ProxyTask::getFallback()" << ENDLOG;
	//wait before retry to recreate proxy
	if (this->retryNr_ == RETRIES_BEFORE_OPEN_CIRCUIT_BREAKER) {
		this->retryNr_ = 0;
		sleep (WAIT_CIRCUIT_BREAKER_OPEN);
	}
	else {
    this->retryNr_++;
		sleep (WAIT_CIRCUIT_BREAKER_CLOSE);
	}
  //Tango::DeviceProxy *proxy = new Tango::DeviceProxy(properties_.device_proxy_name);
  //dev_proxy_.reset(proxy);
}

/////////////////////////////////////////////////////////////////////////////////////
  bool ProxyTask::init_interface()
  {
	DEBUG_STREAM << "Updating 'remoteClass' attribute" << ENDLOG;
	yat::MutexLock guard(init_mutex_);
  bool distInterfaceReady = false;
 
  // Tango bug ? if the property exists with nothing in it, we get a vector of size 1 with an empty string,
  // instead of an empty vector
	Tango::DeviceInfo dev_info_1 = dev_proxy_->info() ;
	std::string distant_device_class_1 = dev_info_1.dev_class;
  
  // std::cout<<"Distant class found : "<<distant_device_class_1<<std::endl;
  // std::cout<<"server_id : "<<dev_info_1.server_id<<std::endl;
  
  
	//FT :: First Check if target device is a proxy, in this case, special treatment :
	//If proxy then check if its interface is initialized !
	if ( distant_device_class_1 == "Proxy" ){
    short interface_ready_state = this->targetProxyInterface();
    
    // Return immediatly, we won't initialize local attributes for nothing...
		if (interface_ready_state == distInterfaceNotInitializedYet){
      distInterfaceReady = false;
      return distInterfaceReady;
		} 
    // Error, if dist class is proxy it must contain attribute interface_ready
    else if (interface_ready_state == attributeNotFindErr){
        std::ostringstream oss;
          oss <<  "Distant class is Proxy but cannot find interface_ready_attr in it \nCheck distant proxy's release..." << std::ends;
		   this->initStatus_.status =   oss.str().c_str();
          throw_devfailed("INIT_ERROR",
                          oss.str().c_str(),
                          "ProxyTask::init_interface");
    }
    //Continue initialisation
    else if (interface_ready_state == distInterfaceInitialized){
      distInterfaceReady = true;
    }
    
	}
  else {
    //For classic devices ping is enough to say that distant interface is initialized
    distInterfaceReady = true;
  }
	
  
    bool empty_attr_name_list = properties_.attr_name_list.empty()
                                  || (properties_.attr_name_list.size() == 1
                                      && properties_.attr_name_list[0].empty());


    if ( empty_attr_name_list )
    {
      DEBUG_STREAM << "AttributeNameList property is empty. No attributes created" << ENDLOG;
    }
    else
    {
      DEBUG_STREAM << "Processing AttributeNameList property..." << ENDLOG;
      
      // get the list of all attributes config of the distant device
      DEBUG_STREAM << "Retrieving complete attributes list from " << properties_.device_proxy_name << ENDLOG;
     boost::scoped_ptr< Tango::AttributeInfoList > attr_config_list_guard( dev_proxy_->attribute_list_query( ) );
     Tango::AttributeInfoList& attr_config_list = *attr_config_list_guard;
     Tango::AttributeInfoList::iterator attr_info_it;
	 
	 
	 // remove the "State" attribute
      DEBUG_STREAM << "Removing 'State' if present" << ENDLOG;
      erase_if(attr_config_list,
               boost::bind(equals_nocase(),
                           std::string("State"),
                           boost::bind(&Tango::AttributeInfo::name, _1)) );

      // remove the "Status" attribute
      DEBUG_STREAM << "Removing 'Status' if present" << ENDLOG;
      erase_if(attr_config_list,
               boost::bind(equals_nocase(),
                           std::string("Status"),
                           boost::bind(&Tango::AttributeInfo::name, _1)) );
      
      // remove the "errorAttributeReport" attribute
      DEBUG_STREAM << "Removing 'errorAttributeReport' if present" << ENDLOG;
      erase_if(attr_config_list,
               boost::bind(equals_nocase(),
                           std::string("errorAttributeReport"),
                           boost::bind(&Tango::AttributeInfo::name, _1)) );
      
      // remove the "errorCommandReport" attribute
      DEBUG_STREAM << "Removing 'errorCommandReport' if present" << ENDLOG;
      erase_if(attr_config_list,
               boost::bind(equals_nocase(),
                           std::string("errorCommandReport"),
                           boost::bind(&Tango::AttributeInfo::name, _1)) );
      DEBUG_STREAM << "Removing 'read_only' if present" << ENDLOG;
      erase_if(attr_config_list,
               boost::bind(equals_nocase(),
                           std::string("read_only"),
                           boost::bind(&Tango::AttributeInfo::name, _1)) );

      DEBUG_STREAM << "Removing 'read_only_2' if present" << ENDLOG;
      erase_if(attr_config_list,
               boost::bind(equals_nocase(),
                           std::string("read_only_2"),
                           boost::bind(&Tango::AttributeInfo::name, _1)) );
                           
      DEBUG_STREAM << "Removing 'log' if present" << ENDLOG;
      erase_if(attr_config_list,
               boost::bind(equals_nocase(),
                           std::string("log"),
                           boost::bind(&Tango::AttributeInfo::name, _1)) );                  
               
      DEBUG_STREAM << "Removing 'info' if present" << ENDLOG;
      erase_if(attr_config_list,
               boost::bind(equals_nocase(),
                           std::string("info"),
                           boost::bind(&Tango::AttributeInfo::name, _1)) );                  
               
      DEBUG_STREAM << "Removing 'interface_ready' if present" << ENDLOG;
      erase_if(attr_config_list,
               boost::bind(equals_nocase(),
                           std::string("interface_ready"),
                           boost::bind(&Tango::AttributeInfo::name, _1)) );

				// remove the "remoteclass" attribute
			DEBUG_STREAM << "Removing 'remoteclass' if present" << ENDLOG;
			erase_if(attr_config_list,
				boost::bind(equals_nocase(),
				std::string("remoteClass"),
				boost::bind(&Tango::AttributeInfo::name, _1)) );

      // transform the '*'
      if ( boost::algorithm::starts_with( properties_.attr_name_list[0], "*" ) )
      {
        DEBUG_STREAM << "Taking the complete attribute set (property is '*')" << ENDLOG;
        bool read_only = (properties_.attr_name_list[0] == "*,R");
        if (read_only)
        {
          DEBUG_STREAM << "Forcing all attributes to be READ ONLY" << ENDLOG;
        }
        properties_.attr_name_list.clear();
        BOOST_FOREACH( Tango::AttributeInfo& attr_info, attr_config_list )
        {
          properties_.attr_name_list.push_back( attr_info.name + "," + attr_info.name + (read_only ? ",R" : "") );
        }
      }

      if (properties_.update_properties)
      {
        DEBUG_STREAM << "Updating properties..." << ENDLOG;
        Tango::DbData properties_to_be_written;
        Tango::DbDatum datum( "AttributeNameList" );
        datum << properties_.attr_name_list;
        properties_to_be_written.push_back( datum );
        device_->get_db_device()->put_property(properties_to_be_written);
      }


      std::vector<AttrDesc> attributes_list;
      for (size_t i = 0; i < properties_.attr_name_list.size(); i++)
      {
        // 'attr_entry' should be parsed as : attribute name on this device, underlying attribute name(, R)
        std::string attr_entry = properties_.attr_name_list[i];
      
        DEBUG_STREAM << "Processing : " << attr_entry << ENDLOG;
        
        // first remove any space for security
        boost::erase_all( attr_entry, " " );

        // extract the 2 or 3 parts of the configuration
        std::vector< std::string > splitted_attr_entry;
        boost::split( splitted_attr_entry, attr_entry, boost::is_any_of(",") );

        if (splitted_attr_entry.size() < 2 || splitted_attr_entry.size() > 4)
        {
          std::string error = "Malformed entry. Should be : {attribute name}, {distant attribute name} (, {R})(, {OPERATOR/EXPERT})";
          ERROR_STREAM << error << ENDLOG;
          this->initStatus_.status =  "Malformed entry. Should be : {attribute name}, {distant attribute name} (, {R})(, {OPERATOR/EXPERT})";
          throw_devfailed("INIT_ERROR",
                          error.c_str(),
                          "ProxyTask::init_interface");
        }

        std::string& local_attr_name   = splitted_attr_entry[0];
        std::string& distant_attr_name = splitted_attr_entry[1];

        if ( distant_attr_name == "State" || distant_attr_name == "Status" || distant_attr_name == "log" || distant_attr_name == "info")
        {
          std::ostringstream oss;
          oss <<  "Attribute " << distant_attr_name << " cannot be in the AttributeNameList property. You must remove it" << std::ends;
		   this->initStatus_.status =   oss.str().c_str();
          throw_devfailed("INIT_ERROR",
                          oss.str().c_str(),
                          "ProxyTask::init_interface");
        }

        // check that the distant_attr_name really exists in the underlying device
        Tango::AttributeInfoList::iterator attr_config_it;
        attr_config_it = std::find_if( attr_config_list.begin(),
                                       attr_config_list.end(),
                                       boost::bind(equals_nocase(),
                                                   distant_attr_name,
                                                   boost::bind(&Tango::AttributeInfo::name, _1)) );
	   
	   	

        if ( attr_config_it == attr_config_list.end() )
        {
          std::ostringstream oss;
          oss <<  "Attribute " << distant_attr_name << " not found in " << properties_.device_proxy_name << std::ends;
          ERROR_STREAM << oss.str() << ENDLOG;
        this->initStatus_.status =    oss.str().c_str();
          throw_devfailed("INIT_ERROR",
                          oss.str().c_str(),
                          "ProxyTask::init_interface");
        }

        AttrDesc attr_desc;
        attr_desc.distant_attr_info = *attr_config_it;
        if ( attr_desc.distant_attr_info.writable == Tango::READ_WITH_WRITE )
        {
          WARN_STREAM << "Skipping " << distant_attr_name << " (READ_WITH_WRITE attributes not supported)" << ENDLOG;
		  	
          continue;
        }

        if ( splitted_attr_entry.size() >= 3 )
        {
          std::string third = boost::to_upper_copy(splitted_attr_entry[2]);
          if (third == "R")
          {
            // do as if it was a read only attribute
            attr_desc.distant_attr_info.writable = Tango::READ;
          }
        }

        if ( splitted_attr_entry.size() == 4 )
        {
          std::string fourth = boost::to_upper_copy(splitted_attr_entry[3]);
          if (fourth == "OPERATOR")
          {
            // do as if it was a read only attribute
            attr_desc.distant_attr_info.disp_level = Tango::OPERATOR;
          }
          else if (fourth == "EXPERT")
          {
            // do as if it was a read only attribute
            attr_desc.distant_attr_info.disp_level = Tango::EXPERT;
          }
        }

        attr_desc.attr = ProxyInterfaceFactory::instance().create_attr( local_attr_name, attr_desc.distant_attr_info, cache_ );
        
        if (!attr_desc.attr)
        {
          ERROR_STREAM << "Unable to create attribute " << local_attr_name << ENDLOG;
          continue;
        }
        attr_desc.converter = ConverterFactory::instance().create( attr_desc.distant_attr_info.data_type, attr_desc.distant_attr_info.data_format );
        attributes_list.push_back( attr_desc );
      }
      // all attributes created : now add them to the device
      BOOST_FOREACH( AttrDesc& attr_desc, attributes_list )
      {
        Tango::Attr* tango_attr = attr_desc.attr->get_tango_interface();
        string attributeName = tango_attr->get_name();
        device_->add_attribute( tango_attr );
        attributes_list_.push_back( attr_desc );
        attributes_to_read_.push_back( attr_desc.distant_attr_info.name );
      }
	  
    }
    
	// 
	// Commands creation
	// 
    bool empty_command_name_list = properties_.command_name_list.empty()
                                  || (properties_.command_name_list.size() == 1 && properties_.command_name_list[0] == "");

    if ( empty_command_name_list )
    {
      DEBUG_STREAM << "CommandNameList property is empty. No commands created" << ENDLOG;
    }
    else
    { 
      DEBUG_STREAM << "Processing CommandNameList property..." << ENDLOG;

      boost::scoped_ptr< Tango::CommandInfoList > cmd_info_list_guard( dev_proxy_->command_list_query() );
      Tango::CommandInfoList& cmd_info_list = *cmd_info_list_guard;

/*
      erase_if(cmd_info_list,
               boost::bind(equals_nocase(),
                           std::string("Init"),
                           boost::bind(&Tango::CommandInfo::cmd_name, _1)) );
*/
      erase_if(cmd_info_list,
               boost::bind(equals_nocase(),
                           std::string("State"),
                           boost::bind(&Tango::CommandInfo::cmd_name, _1)) );
      erase_if(cmd_info_list,
               boost::bind(equals_nocase(),
                           std::string("Status"),
                           boost::bind(&Tango::CommandInfo::cmd_name, _1)) );

      if ( properties_.command_name_list[0] == "*" )
      {
        properties_.command_name_list.clear();
        BOOST_FOREACH( const Tango::CommandInfo& cmd_info, cmd_info_list )
        {
			
          if (cmd_info.cmd_name != "Init")
            properties_.command_name_list.push_back( cmd_info.cmd_name + "," + cmd_info.cmd_name );
        }
      }

      if (properties_.update_properties)
      {
        DEBUG_STREAM << "Updating properties..." << ENDLOG;
        Tango::DbData properties_to_be_written;
        Tango::DbDatum datum( "CommandNameList" );
        datum << properties_.command_name_list;
        properties_to_be_written.push_back( datum );
        device_->get_db_device()->put_property(properties_to_be_written);
      }

      for (size_t i = 0; i < properties_.command_name_list.size(); i++)
      {
        // 'cmd_entry' should be parsed as : command name on this device, underlying command name
        std::string cmd_entry = properties_.command_name_list[i];
      
        DEBUG_STREAM << "Processing : " << cmd_entry << ENDLOG;
        
        // first remove any space for security
        boost::erase_all( cmd_entry, " " );

        // extract the 2 or 3 parts of the configuration
        std::vector< std::string > splitted_cmd_entry;
        boost::split( splitted_cmd_entry, cmd_entry, boost::is_any_of(",") );

        if (splitted_cmd_entry.size() != 2)
        {
          std::string error = "Malformed entry. Should be : {command name}, {distant command name}";
          ERROR_STREAM << error << ENDLOG;
          this->initStatus_.status =   "Malformed entry. Should be : {command name}, {distant command name}" ;
          throw_devfailed("INIT_ERROR",
                          error.c_str(),
                          "ProxyTask::init_interface");
        }

        std::string& local_cmd_name   = splitted_cmd_entry[0];
        std::string& distant_cmd_name = splitted_cmd_entry[1];

        // check that the distant_cmd_name really exists in the underlying device
        Tango::CommandInfoList::iterator cmd_info_it;
        cmd_info_it = std::find_if( cmd_info_list.begin(),
                                    cmd_info_list.end(),
                                    boost::bind(equals_nocase(),
                                                distant_cmd_name,
                                                boost::bind(&Tango::CommandInfo::cmd_name, _1)) );
        if ( cmd_info_it == cmd_info_list.end() )
        {
		
          std::ostringstream oss;
          oss <<  "Command " << distant_cmd_name << " not found in " << properties_.device_proxy_name << std::ends;
          ERROR_STREAM << oss.str() << ENDLOG;
		  this->initStatus_.status =   oss.str().c_str() ;
          throw_devfailed("INIT_ERROR",
                          oss.str().c_str(),
                          "ProxyTask::init_interface");
        }

      ProxyCmdP proxy_cmd( ProxyInterfaceFactory::instance().create_cmd( local_cmd_name, *cmd_info_it, dev_proxy_, device_) );
      
      commands_list_.push_back( proxy_cmd );
      }

      // all commands created : now add them to the device
      BOOST_FOREACH( ProxyCmdP& proxy_cmd, commands_list_ )
      {
        Tango::Command* tango_cmd = proxy_cmd->get_tango_interface();
        DEBUG_STREAM << "Adding " << tango_cmd->get_name() << " to the command list" << ENDLOG;
        std::vector< Tango::Command* >& command_list = device_->get_device_class()->get_command_list();
        command_list.push_back( tango_cmd );
      }
    }

	// 
	// Tango interface is OK
	// -> update the remoteClass attribute
	// 
		DEBUG_STREAM << "Updating 'remoteClass' attribute" << ENDLOG;
		
		Tango::DeviceInfo dev_info = dev_proxy_->info() ;

		// Debug info for TANGODEVIC-765
		std::stringstream s;
		s << " dev_info.dev_class=" 	<< dev_info.dev_class << ",";
		s << " dev_info.server_id=" 	<< dev_info.server_id << ",";
		s << " dev_info.server_version=" << dev_info.server_version << ",";
		s << " dev_info.dev_type=" 	<< dev_info.dev_type << ",";
		s << " dev_info.server_host=" 	<< dev_info.server_host << ",";
	
		std::string distant_device_class = dev_info.dev_class;

		
		Tango::DbData	dev_prop;
		dev_prop.push_back(Tango::DbDatum("_DeviceProxyClass"));

		if ( distant_device_class == "Proxy" )
		{
			DEBUG_STREAM << properties_.device_proxy_name
				<< " is already a Proxy. Retrieving its '_DeviceProxyClass' property"
				<< ENDLOG;
			// directly take the underlying class
			dev_proxy_->get_property( dev_prop );
			// directly take the underlying class
			if ( dev_prop[0].is_empty() )
			{
				std::ostringstream oss;
				oss << "The device "
					<< properties_.device_proxy_name
					<< " is of type 'Proxy' but does NOT have a '_DeviceProxyClass' property. "
					<< properties_.device_proxy_name
					<< " must be initialized or updated;"
					<< "the remote device info is the following " <<  s.str ().c_str ();
				
				this->initStatus_.status =    oss.str().c_str() ;
				throw_devfailed("INIT_ERROR", oss.str().c_str(), "ProxyTask::init_interface");
			}
			else
			{
				dev_prop[0] >> distant_device_class;
			}
		}
		
//Store class of remote device in a property 
		DEBUG_STREAM << properties_.device_proxy_name
			<< " is a "
			<< distant_device_class
			<< ENDLOG;

		dev_prop[0] << distant_device_class;
		device_->get_db_device()->put_property( dev_prop );

// Store also the remote device in an attribute 
		Proxy_ns::Proxy * p = 0;
			try 
			{
				p = reinterpret_cast<Proxy_ns::Proxy*> (device_);
			}
			catch (...)
			{
				throw_devfailed("EXECUTION_ERROR",
					"reinterpret_cast<ProxyTask::init_interface>  failed",
					"ProxyTask::init_interface");
			}
  p ->set_remoteClass(distant_device_class); 

  return distInterfaceReady;
  }
//**********************************************************************************
//
//  ProxyTask::update_read_values()
//
//**********************************************************************************

  void ProxyTask::update_read_values()
  {
    
    //FT refresh state/status first : INFINTER-694
     {//- critical section
//      yat::MutexLock guard(state_status_mutex_);
      yat::MutexLock guard ( cache_->get_cache_mutex() );
      state_ = dev_proxy_->state();
      status_ = dev_proxy_->status();
    //}
      //First check if attribute list is not empty : TANGODEVIC-1653
      if (attributes_to_read_.size() > 0 ){
        std::vector<Tango::DeviceAttribute> * dev_attrs = 0;
        // then read remote attributes 
        try
        {
          dev_attrs = dev_proxy_->read_attributes(attributes_to_read_);

          // Check if all attributes have been read
          if (attributes_list_.size() != (*dev_attrs).size())
          {
            ERROR_STREAM << "ProxyTask::update_read_values::unexpected reply content from device!" << ENDLOG;
            this->set_invalid_read_values();
            {
              yat::MutexLock guard(state_status_mutex_);
              state_ = Tango::FAULT;
              status_ = "ProxyTask::update_read_values::unexpected reply content from device!";
            }
            return;
          }
          // directly take the underlying class
          // Extract the values of the attributes read
          for (size_t i = 0; i < attributes_list_.size(); i++)
          {
            AttrDesc & attr_desc = attributes_list_[i];

            Value value;

            try
            {
              value.data = attr_desc.converter->extract((*dev_attrs)[i]);
              value.quality = (*dev_attrs)[i].quality;
              value.timestamp = (*dev_attrs)[i].time;
            }
            catch( Tango::DevFailed& df )
            {
              ERROR_STREAM << "Error caught when extracting data from "
                           << attr_desc.attr->get_tango_interface()->get_name()
                           << " : "
                           << ENDLOG;
              ERROR_STREAM << df << ENDLOG;

              value.data = boost::any();
              value.quality = Tango::ATTR_INVALID;
              value.timestamp = (*dev_attrs)[i].time;
            }
            // publish Data in the cache
            cache_->publish_new_data( attr_desc.attr->get_tango_interface()->get_name(), value );
          }
        }
        catch (...)
        {
          ERROR_STREAM << "ProxyTask::update_read_values() : Caught (..)  "  << ENDLOG;
          delete dev_attrs;
          throw;
        }
        // clear DeviceAttribute vector
        delete dev_attrs;
      
      }
      
    }//- end critical section
  }
//**********************************************************************************
//
//  ProxyTask::set_invalid_read_values()
//
//**********************************************************************************

  void ProxyTask::set_invalid_read_values()
  {
    DEBUG_STREAM << "void ProxyTask::set_invalid_read_values()" << ENDLOG;
    {//- critical section
      yat::MutexLock guard ( cache_->get_cache_mutex() );
      for (size_t i = 0; i < attributes_list_.size(); i++)
      {
        AttrDesc& attr_desc = attributes_list_[i];
        Value value;
        value.data = boost::any(); // empty data
        value.quality = Tango::ATTR_INVALID; // invalid data
        value.timestamp.tv_sec  = 0;
        value.timestamp.tv_usec = 0;
        value.timestamp.tv_nsec = 0;

        cache_->publish_new_data( attr_desc.attr->get_tango_interface()->get_name(), value );
      }

      std::ostringstream status;
      status << "Unable to read data since "
             << last_valid_update_time_.str_elapsed_ms()
             << std::endl
             << "Last error : "
             << last_error_.errors[0].desc
             << std::ends;

      //{
      //  yat::MutexLock guard(state_status_mutex_);
        state_  = Tango::FAULT;
        status_ = status.str();
    }//- end critical section
  }

//**********************************************************************************
//
//  ProxyTask::get_state_status()
//
//**********************************************************************************

  std::pair< Tango::DevState, std::string > ProxyTask::get_state_status()
  {
    DEBUG_STREAM << "void ProxyTask::get_state_status()" << ENDLOG;
//    yat::MutexLock guard(state_status_mutex_);
      yat::MutexLock guard ( cache_->get_cache_mutex() );
    return std::make_pair( state_, status_ );
  }

//**********************************************************************************
//
//  ProxyTask::targetProxyInterface()
//
//**********************************************************************************  
  short ProxyTask::targetProxyInterface()
  {
    DEBUG_STREAM << "bool ProxyTask::targetProxyInterface()" << ENDLOG;
    bool result = false;
    
    boost::scoped_ptr< Tango::AttributeInfoList > attr_config_list_guard( dev_proxy_->attribute_list_query( ) );
    Tango::AttributeInfoList& attr_config_list = *attr_config_list_guard;

    for(int i1=0; i1<attr_config_list.size(); i1++){
      Tango::AttributeInfo currentRead = attr_config_list[i1];
      if (currentRead.name == ATTR_PROXY_INTERFACE_STATUS_NAME){
        Tango::DeviceAttribute dev_attr = dev_proxy_->read_attribute(currentRead.name);
        dev_attr>>result;
        if (result)
          return distInterfaceInitialized;
        else if (!result)
          return distInterfaceNotInitializedYet;
      }
    }

    return attributeNotFindErr;
  }
//**********************************************************************************
//
//  ProxyTask::write_value()
//
//**********************************************************************************

  void ProxyTask::write_value(const std::string& attr_name, const std::string& distant_attr_name, boost::any data)
  {
    DEBUG_STREAM << "void ProxyTask::write_value()" << ENDLOG;
    try
    {
      WriteValue w_value;
      w_value.attr_name = attr_name;
      w_value.distant_attr_name = distant_attr_name;
      w_value.data = data;

      yat::Message* msg = yat::Message::allocate( MSGID_WRITE_REQUEST, DEFAULT_MSG_PRIORITY, true );
      msg->attach_data( w_value );
 
	  // JIRA TANGODEVIC-1181
      this->wait_msg_handled( msg , properties_.device_proxy_timeout);
    }
    catch( yat::Exception& ex )
    {
      throw_devfailed( ex );
    }
  }
//**********************************************************************************
//
//  ProxyTask::on_write_value()
//
//**********************************************************************************


  void ProxyTask::on_write_value( WriteValue& w_val )
  {
     DEBUG_STREAM << "void ProxyTask::on_write_value()" << ENDLOG;
    try
    {
      std::vector<std::string>::iterator distant_attr_it;
      distant_attr_it = std::find_if( attributes_to_read_.begin(),
                                      attributes_to_read_.end(),
                                      boost::bind( equals_nocase(),
                                                   w_val.distant_attr_name,
                                                   _1) );

      if ( distant_attr_it == attributes_to_read_.end() )
      {
        std::ostringstream oss;
        oss << "Attribute " << w_val.distant_attr_name << " not found in " << dev_proxy_->name() << std::ends;
        throw_devfailed( "ATTR_NOT_FOUND", oss.str().c_str(), "ProxyTask::on_write_value" );
      }

      AttrDesc& desc = attributes_list_[distant_attr_it - attributes_to_read_.begin()];

      Tango::DeviceAttribute dev_attr = desc.converter->insert( w_val.data );
      dev_attr.set_name( w_val.distant_attr_name );
	  	 
	  //Possibility of print writed value when simple data type, scalar data:  (FT CTRLRFC-109)
	  if ( dev_attr.get_data_format()   == Scalar_Data_Format){		
	  
		switch ( dev_attr.get_type())
				{
				case Tango::DEV_SHORT:   
					short a_short;
					dev_attr>>a_short;
					INFO_STREAM << "Writing value of "<< w_val.distant_attr_name<< ", with short value :"<<a_short<<ENDLOG;
				break;
				case Tango::DEV_LONG:   
					long a_long;
					dev_attr>>a_long;
					INFO_STREAM << "Writing value of "<< w_val.distant_attr_name<< ", with long value :"<<a_long<<ENDLOG;
				break;
				case Tango::DEV_DOUBLE: 
					double a_double;
					dev_attr>>a_double;
					INFO_STREAM << "Writing value of "<< w_val.distant_attr_name<< ", with double value :"<<a_double<<ENDLOG;
				break;
				case Tango::DEV_FLOAT:
					float a_float;
					dev_attr>>a_float;
					INFO_STREAM << "Writing value of "<< w_val.distant_attr_name<< ", with float value :"<<a_float<<ENDLOG;
				break;
				case Tango::DEV_BOOLEAN:
					bool a_bool;
					dev_attr>>a_bool;
					INFO_STREAM << "Writing value of "<< w_val.distant_attr_name<< ", with bool value :"<<a_bool<<ENDLOG;
				break;
				case Tango::DEV_USHORT: 
					unsigned short a_uShort;
					dev_attr>>a_uShort;
					INFO_STREAM << "Writing value of "<< w_val.distant_attr_name<< ", with unsigned short value :"<<a_uShort<<ENDLOG;
				break;
				case Tango::DEV_ULONG:  
					unsigned long a_uLong;
					dev_attr>>a_uLong;
					INFO_STREAM << "Writing value of "<< w_val.distant_attr_name<< ", with unsigned  long value :"<<a_uLong<<ENDLOG;
				break;
				case Tango::DEV_UCHAR: 
					unsigned char a_uChar;
					dev_attr>>a_uChar;
					INFO_STREAM << "Writing value of "<< w_val.distant_attr_name<< ", with unsigned char value :"<<a_uChar<<ENDLOG;
				break;
				case Tango::DEV_STRING:
					std::string a_string;
					dev_attr>>a_string;
					INFO_STREAM << "Writing value of "<< w_val.distant_attr_name<< ", with unsigned char value :"<<a_string<<ENDLOG;				
				break;
				
				defaut:
					INFO_STREAM << "Writing value of : " << w_val.distant_attr_name<< ENDLOG;
				
				}
		}
	  else
		INFO_STREAM << "Writing value of : " << w_val.distant_attr_name<< ENDLOG;
		
      dev_proxy_->write_attribute( dev_attr );
    }
    catch( yat::Exception& )
    {
      throw;
    }
    catch( Tango::DevFailed& df )
    {
      throw_yat_exception(df);
    }
    catch( std::exception& ex )
    {
      std::ostringstream oss;
      oss << "std::exception caught when writing value of "
          << w_val.distant_attr_name
          << " : "
          << ex.what()
          << std::ends;
      throw_yat_exception("UNKNOWN_ERROR",oss.str().c_str(),"ProxyTask::on_write_value");
    }
    catch( ... )
    {
      std::ostringstream oss;
      oss << "Unknown error when writing value of "
          << w_val.distant_attr_name
          << std::ends;
      throw_yat_exception("UNKNOWN_ERROR",oss.str().c_str(),"ProxyTask::on_write_value");
    }
  }

}
