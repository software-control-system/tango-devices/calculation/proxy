#ifndef _PROXY_THREAD_DELETER_H
#define _PROXY_THREAD_DELETER_H

#include <boost/noncopyable.hpp>
#include <yat/threading/Thread.h>

namespace Proxy_ns
{

  //! this class is intended to be used as the deleter of
  //! a boost::shared_ptr<yat::Thread> instance to automatically 
  //! and properly exit the thread
  class ThreadExiter
  {
  public:
    void operator ()( yat::Thread* t )
    {
      try
      {
        t->exit();
      }
      catch(...) {}
    }
  };

}

#endif
