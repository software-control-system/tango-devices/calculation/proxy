#include "Cache.h"
#include "Types.h"

namespace Proxy_ns
{
  void throw_yat_exception( const char* reason, const char* desc, const char* origin )
  {
    throw yat::Exception( reason, desc, origin );
  }

  //- Tango::DevFailed -> yat::Exception
  void throw_yat_exception( Tango::DevFailed& df )
  {
    yat::Exception yat_ex;
    for (size_t i = 0; i < df.errors.length(); i++) 
    {
      yat_ex.push_error( df.errors[i].reason,
                         df.errors[i].desc,
                         df.errors[i].origin,
                         -1,
                         df.errors[i].severity);
    }
    throw yat_ex;
  }

  void re_throw_yat_exception( Tango::DevFailed& df, const char* reason, const char* desc, const char* origin )
  {
    yat::Exception yat_ex;
    for (size_t i = 0; i < df.errors.length(); i++) 
    {
      yat_ex.push_error( df.errors[i].reason,
                         df.errors[i].desc,
                         df.errors[i].origin,
                         -1,
                         df.errors[i].severity);
    }
    yat_ex.push_error(reason, desc, origin);
    throw yat_ex;
  }

  //- strings -> Tango::DevFailed
  void throw_devfailed( const char* reason, const char* desc, const char* origin )
  {
    Tango::DevFailed df;
    df.errors.length( 1 );
    df.errors[0].reason   = CORBA::string_dup( reason );
    df.errors[0].desc     = CORBA::string_dup( desc   );
    df.errors[0].origin   = CORBA::string_dup( origin );
    df.errors[0].severity = Tango::ERR;
    throw df;
  }

  //- yat::Exception -> Tango::DevFailed
  void throw_devfailed( yat::Exception& yat_ex)
  {
    Tango::DevFailed df;
    const yat::Exception::ErrorList& yat_errors = yat_ex.errors;
    df.errors.length( yat_errors.size() );
    for (size_t i = 0; i < yat_errors.size(); i++)
    {
      df.errors[i].reason   = CORBA::string_dup( yat_errors[i].reason.c_str() );
      df.errors[i].desc     = CORBA::string_dup( yat_errors[i].desc.c_str()   );
      df.errors[i].origin   = CORBA::string_dup( yat_errors[i].origin.c_str() );
      df.errors[i].severity = static_cast<Tango::ErrSeverity>(yat_errors[i].severity);
    }
    throw df;
  }

  void rethrow_devfailed( yat::Exception& yat_ex, const char* reason, const char* desc, const char* origin )
  {
    Tango::DevFailed df;
    const yat::Exception::ErrorList& yat_errors = yat_ex.errors;
    df.errors.length( yat_errors.size() + 1 );
    size_t i;
    for (i = 0; i < yat_errors.size(); i++)
    {
      df.errors[i].reason   = CORBA::string_dup( yat_errors[i].reason.c_str() );
      df.errors[i].desc     = CORBA::string_dup( yat_errors[i].desc.c_str()   );
      df.errors[i].origin   = CORBA::string_dup( yat_errors[i].origin.c_str() );
      df.errors[i].severity = static_cast<Tango::ErrSeverity>(yat_errors[i].severity);
    }
    df.errors[i].reason   = CORBA::string_dup( reason );
    df.errors[i].desc     = CORBA::string_dup( desc   );
    df.errors[i].origin   = CORBA::string_dup( origin );
    df.errors[i].severity = Tango::ERR;
    throw df;
  }


}
