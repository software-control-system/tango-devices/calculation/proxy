#ifndef _PROXY_CACHE_H_
#define _PROXY_CACHE_H_

#include <boost/any.hpp>
#include <boost/function.hpp>
#include <tango.h>
#include <yat/threading/Mutex.h>

namespace Proxy_ns
{

  struct Value
  {
    Value() {
      //- noop
    };
    Value (const Value & src) { 
      *this = src; 
    }
    const Value & operator= (const Value & src) {
      if (this == &src)
        return *this;
      data = src.data;
      quality = src.quality;
      ::memcpy(&timestamp, &src.timestamp, sizeof(Tango::TimeVal));
      return *this;
    }
    boost::any data;
    Tango::AttrQuality quality;
    Tango::TimeVal timestamp;
  };

  typedef boost::function<void (const std::string& attr_name,
                                const std::string& distant_attr_name,
                                boost::any data)> WriteFunction;

  class Cache
  {
  public:

    // structors
    Cache( WriteFunction write_function );
    ~Cache();

    // called from the task to publish the new data that have been read asynchronously
    void publish_new_data( const std::string& attr_name, Value& new_data );

    // called from the ProxyAttr instance to get the data to output
    Value get_cached_data( const std::string& attr_name );

    // 
    void on_write( const std::string& attr_name,
                   const std::string& distant_attr_name,
                   boost::any data );
                   
    yat::Mutex& get_cache_mutex() {
      return this->mutex;
    }

  private:

    WriteFunction write_function_;

    yat::Mutex mutex;
    std::map< std::string, Value > cached_data_;

  };

  typedef boost::shared_ptr< Cache > CacheP;

}

#endif
