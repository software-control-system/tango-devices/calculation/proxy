#ifndef _PROXY_EXCEPTION_H_
#define _PROXY_EXCEPTION_H_

#include <tango.h>

namespace Proxy_ns
{

  //- Tango::DevFailed -> yat::Exception
  void throw_yat_exception( const char*, const char*, const char* );

  void throw_yat_exception( Tango::DevFailed& );

  void re_throw_yat_exception( Tango::DevFailed&, const char*, const char*, const char* );

  //- yat::Exception -> Tango::DevFailed
  void throw_devfailed( const char*, const char*, const char* );

  void throw_devfailed( yat::Exception& );

  void rethrow_devfailed( yat::Exception&, const char*, const char*, const char* );

}

#endif
