#include "Converter.h"
#include "Types.h"

namespace Proxy_ns
{

  template <typename T>
  struct ScalarConverter : public Converter
  {
    boost::any extract( Tango::DeviceAttribute& dev_attr )
    {
      boost::any result;
      try
      {
        bool not_empty = ! dev_attr.is_empty();
        bool successfully_read = ! dev_attr.has_failed();
        bool has_compatible_type = dev_attr.get_type() == TangoTraits<T>::type_id;
        if ( not_empty &&  successfully_read && has_compatible_type)
        {
          T value;
          dev_attr >> value;
          result = value;
        }
      }  
      catch (...)
      {
        //- ignore extraction error and return empty boost::any 
      }
      return result;
    }

    Tango::DeviceAttribute insert( boost::any& value )
    {
      Tango::DeviceAttribute result;
      try
      {
        T scalar = boost::any_cast<T>( value );
        result << scalar;
      }
      catch( boost::bad_any_cast )
      {
        //- unable to extract value!
        //- ignore extraction error and return empty Tango::DeviceAttribute
      }
      return result;
    }
  };


  struct StringScalarConverter : public Converter
  {
    boost::any extract( Tango::DeviceAttribute& dev_attr )
    {
      boost::any result;
      try
      {
        bool successfully_read = ! dev_attr.has_failed();
        bool not_empty = ! dev_attr.is_empty();
        bool has_compatible_type = dev_attr.get_type() == Tango::DEV_STRING;
        if ( not_empty &&  successfully_read && has_compatible_type )
        {
          std::string value;
          dev_attr >> value;
          result = value;
        }
      }
      catch (...)
      {
        //- ignore extraction error and return empty boost::any 
      }
      return result;
    }

    Tango::DeviceAttribute insert( boost::any& value )
    {
      Tango::DeviceAttribute result;
      try
      {
        std::string& scalar = boost::any_cast<std::string&>( value );
        result << scalar;
      }
      catch( boost::bad_any_cast )
      {
        // unable to extract value ?
      }
      return result;
    }
  };


  template <typename T>
  struct SpectrumConverter : public Converter
  {
    typedef typename Types<T>::Buffer  Buffer;
    typedef typename Types<T>::BufferP BufferP;

    boost::any extract( Tango::DeviceAttribute& dev_attr )
    {
      boost::any result;
      try
      {
        bool not_empty = ! dev_attr.is_empty();
        bool successfully_read = ! dev_attr.has_failed();
        bool has_compatible_type = dev_attr.get_type() == TangoTraits<T>::type_id;
        if ( not_empty &&  successfully_read && has_compatible_type )
        {
          BufferP buf( new Buffer(boost::extents[dev_attr.dim_x]));
          std::copy( TangoTraits<T>::internal_buffer(dev_attr)->get_buffer(),
                     TangoTraits<T>::internal_buffer(dev_attr)->get_buffer() + dev_attr.dim_x,
                     buf->data());
          result = buf;
        }
      }
      catch (...)
      {
        //- ignore extraction error and return empty boost::any 
      }
      return result;
    }

    Tango::DeviceAttribute insert( boost::any& value )
    {
      // writing spectrum not supported yet
      return Tango::DeviceAttribute();
    }

  };

  struct StringSpectrumConverter : public Converter
  {
    typedef boost::multi_array<std::string, 1> Buffer;
    typedef boost::shared_ptr<Buffer>          BufferP;

    boost::any extract( Tango::DeviceAttribute& dev_attr )
    {
      boost::any result;
      try
      {
        bool not_empty = ! dev_attr.is_empty();
        bool successfully_read = ! dev_attr.has_failed();
        bool has_compatible_type = dev_attr.get_type() == Tango::DEV_STRING;
        if ( not_empty &&  successfully_read && has_compatible_type )
        {
          BufferP buf( new Buffer(boost::extents[dev_attr.dim_x]) );
          std::copy( dev_attr.StringSeq->get_buffer(),
                     dev_attr.StringSeq->get_buffer() + dev_attr.dim_x,
                     buf->data());
          result = buf;
        }
      }
      catch (...)
      {
        //- ignore extraction error and return empty boost::any 
      }
      return result;
    }

    Tango::DeviceAttribute insert( boost::any& value )
    {
      // writing spectrum not supported yet
      return Tango::DeviceAttribute();
    }

  };

  template <typename T>
  struct ImageConverter : public Converter
  {
    boost::any extract( Tango::DeviceAttribute& dev_attr )
    {
      boost::any result;
      try
      {
        bool not_empty = ! dev_attr.is_empty();
        bool successfully_read = ! dev_attr.has_failed();
        bool has_compatible_type = dev_attr.get_type() == TangoTraits<T>::type_id;
        if ( not_empty &&  successfully_read && has_compatible_type )
        {
          typename Types<T>::ImageP buf( new typename Types<T>::Image(boost::extents[dev_attr.dim_y][dev_attr.dim_x]) );
          std::copy( TangoTraits<T>::internal_buffer(dev_attr)->get_buffer(),
                     TangoTraits<T>::internal_buffer(dev_attr)->get_buffer() + dev_attr.dim_x * dev_attr.dim_y,
                     buf->data() );
          result = buf;
        }
      }
      catch (...)
      {
        //- ignore extraction error and return empty boost::any 
      }
      return result;
    }

    Tango::DeviceAttribute insert( boost::any& value )
    {
      // writing images not supported yet
      return Tango::DeviceAttribute();
    }
  };

  struct StringImageConverter : public Converter
  {
    typedef boost::multi_array<std::string, 2> Buffer;
    typedef boost::shared_ptr<Buffer>          BufferP;

    boost::any extract( Tango::DeviceAttribute& dev_attr )
    {
      boost::any result;
      try
      {
        bool not_empty = ! dev_attr.is_empty();
        bool successfully_read = ! dev_attr.has_failed();
        bool has_compatible_type = dev_attr.get_type() == Tango::DEV_STRING;
        if ( not_empty &&  successfully_read && has_compatible_type )
        {
          BufferP buf( new Buffer(boost::extents[dev_attr.dim_y][dev_attr.dim_x]) );
          std::copy( dev_attr.StringSeq->get_buffer(),
                     dev_attr.StringSeq->get_buffer() + dev_attr.dim_x * dev_attr.dim_y,
                     buf->data() );
          result = buf;
        }
      }
      catch (...)
      {
        //- ignore extraction error and return empty boost::any 
      }
      return result;
    }

    Tango::DeviceAttribute insert( boost::any& value )
    {
      // writing images is not supported yet
      return Tango::DeviceAttribute();
    }

  };

  ConverterP ConverterFactory::create( int data_type, Tango::AttrDataFormat format )
  {
    ConverterP extractor;

    switch (format)
    {
      case Tango::SCALAR:
        {
          switch (data_type)
          {
            case Tango::DEV_BOOLEAN : extractor.reset( new ScalarConverter<Tango::DevBoolean>() ); break;
            case Tango::DEV_UCHAR   : extractor.reset( new ScalarConverter<Tango::DevUChar>  () ); break;
            case Tango::DEV_USHORT  : extractor.reset( new ScalarConverter<Tango::DevUShort> () ); break;
            case Tango::DEV_SHORT   : extractor.reset( new ScalarConverter<Tango::DevShort>  () ); break;
            case Tango::DEV_ULONG   : extractor.reset( new ScalarConverter<Tango::DevULong>  () ); break;
            case Tango::DEV_ULONG64 : extractor.reset( new ScalarConverter<Tango::DevULong64>() ); break;
            case Tango::DEV_LONG64  : extractor.reset( new ScalarConverter<Tango::DevLong64> () ); break;
            case Tango::DEV_LONG    : extractor.reset( new ScalarConverter<Tango::DevLong>   () ); break;
            case Tango::DEV_FLOAT   : extractor.reset( new ScalarConverter<Tango::DevFloat>  () ); break;
            case Tango::DEV_DOUBLE  : extractor.reset( new ScalarConverter<Tango::DevDouble> () ); break;
            case Tango::DEV_STRING  : extractor.reset( new StringScalarConverter             () ); break;
          }
        }
        break;
      case Tango::SPECTRUM:
        {
          switch (data_type)
          {
            case Tango::DEV_BOOLEAN : extractor.reset( new SpectrumConverter<Tango::DevBoolean>() ); break;
            case Tango::DEV_UCHAR   : extractor.reset( new SpectrumConverter<Tango::DevUChar>  () ); break;
            case Tango::DEV_USHORT  : extractor.reset( new SpectrumConverter<Tango::DevUShort> () ); break;
            case Tango::DEV_SHORT   : extractor.reset( new SpectrumConverter<Tango::DevShort>  () ); break;
            case Tango::DEV_ULONG   : extractor.reset( new SpectrumConverter<Tango::DevULong>  () ); break;
            case Tango::DEV_LONG    : extractor.reset( new SpectrumConverter<Tango::DevLong>   () ); break;
            case Tango::DEV_ULONG64 : extractor.reset( new SpectrumConverter<Tango::DevULong64>() ); break;
            case Tango::DEV_LONG64  : extractor.reset( new SpectrumConverter<Tango::DevLong64> () ); break;
            case Tango::DEV_FLOAT   : extractor.reset( new SpectrumConverter<Tango::DevFloat>  () ); break;
            case Tango::DEV_DOUBLE  : extractor.reset( new SpectrumConverter<Tango::DevDouble> () ); break;
            case Tango::DEV_STRING  : extractor.reset( new StringSpectrumConverter             () ); break;
          }
        }
        break;
      case Tango::IMAGE:
        {
          switch (data_type)
          {
            case Tango::DEV_BOOLEAN : extractor.reset( new ImageConverter<Tango::DevBoolean>() ); break;
            case Tango::DEV_UCHAR   : extractor.reset( new ImageConverter<Tango::DevUChar>  () ); break;
            case Tango::DEV_USHORT  : extractor.reset( new ImageConverter<Tango::DevUShort> () ); break;
            case Tango::DEV_SHORT   : extractor.reset( new ImageConverter<Tango::DevShort>  () ); break;
            case Tango::DEV_ULONG   : extractor.reset( new ImageConverter<Tango::DevULong>  () ); break;
            case Tango::DEV_LONG    : extractor.reset( new ImageConverter<Tango::DevLong>   () ); break;
            case Tango::DEV_ULONG64 : extractor.reset( new ImageConverter<Tango::DevULong64>() ); break;
            case Tango::DEV_LONG64  : extractor.reset( new ImageConverter<Tango::DevLong64> () ); break;
            case Tango::DEV_FLOAT   : extractor.reset( new ImageConverter<Tango::DevFloat>  () ); break;
            case Tango::DEV_DOUBLE  : extractor.reset( new ImageConverter<Tango::DevDouble> () ); break;
            case Tango::DEV_STRING  : extractor.reset( new StringImageConverter             () ); break;
          }
        }
        break;
    }
    return extractor;
  }

}
