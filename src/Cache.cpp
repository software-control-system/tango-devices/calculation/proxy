#include "Cache.h"
#include "Types.h"

namespace Proxy_ns
{

  Cache::Cache( WriteFunction write_function )
    : write_function_(write_function)
  {
  }

  Cache::~Cache()
  {
  }

  void Cache::publish_new_data( const std::string& attr_name, Value& new_data )
  {
    //yat::MutexLock guard(mutex);
    //- NOTE : mutex is now locked by caller ( i.e. ProxyTask::update_read_values() )
    cached_data_[ attr_name ] = new_data;
  }

  Value Cache::get_cached_data( const std::string& attr_name )
  {
    yat::MutexLock guard(mutex);
    return cached_data_[ attr_name ];
  }

  void Cache::on_write( const std::string& attr_name, const std::string& distant_attr_name, boost::any data )
  {
    write_function_( attr_name, distant_attr_name, data );
  }

}
