#ifndef _EXTRACTOR_H_
#define _EXTRACTOR_H_

#include <boost/noncopyable.hpp>
#include <boost/any.hpp>
#include <tango.h>
#include <yat/utils/Singleton.h>

namespace Proxy_ns
{

  struct Converter
  {
    virtual boost::any extract( Tango::DeviceAttribute& dev_attr ) = 0;
    
    virtual Tango::DeviceAttribute insert( boost::any& value ) = 0;
  };

  typedef boost::shared_ptr< Converter > ConverterP;


  struct ConverterFactory : public yat::Singleton<ConverterFactory>,
                            private boost::noncopyable
  {
    ConverterP create( int data_type, Tango::AttrDataFormat format );
  };



}

#endif
