#include "ProxyInterface.h"
#include "Types.h"
#include "Cache.h"
#include "Proxy.h"
#include "Exception.h"
#include <yat/utils/Logging.h>

// A dummy note as a workaround for SVN corruption
// See TANGODEVIC-331 for more information

namespace Proxy_ns
{
	ProxyAttr::~ProxyAttr()
	{
	}

	ProxyCmd::~ProxyCmd()
	{
	}


	template< typename T >
	class ScalarAttr : public Tango::Attr,
		public ProxyAttr
	{
	public: //- [typedefs]

	public: //- [structors]
		ScalarAttr( CacheP cache,
			const std::string& name,
			const Tango::AttributeInfo& info )
			: Tango::Attr( name.c_str(), TangoTraits<T>::type_id, info.disp_level, info.writable ),
			cache_(cache),
			distant_attr_info_(info)
		{
			Tango::UserDefaultAttrProp	prop;
			prop.set_label        ( info.name.c_str() );
			prop.set_description  ( info.description.c_str() );
			prop.set_unit         ( info.unit.c_str() );
			prop.set_standard_unit( info.standard_unit.c_str() );
			prop.set_display_unit ( info.display_unit.c_str() );
			prop.set_format       ( info.format.c_str() );
			/*
			prop.set_min_value    ( info.min_value.c_str() );
			prop.set_max_value    ( info.max_value.c_str() );

			prop.set_min_alarm    ( info.alarms.min_alarm.c_str() );
			prop.set_max_alarm    ( info.alarms.max_alarm.c_str() );
			prop.set_min_warning  ( info.alarms.min_warning.c_str() );
			prop.set_max_warning  ( info.alarms.max_warning.c_str() );
			*/
			this->set_default_properties(prop);
		}

		~ScalarAttr()
		{
			cache_.reset();
		}

	public: //- [Tango::Attr impl]

		virtual void read(Tango::DeviceImpl *dev, Tango::Attribute &att)
		{
			T* data = boost::any_cast<T>( &value_.data );
			if (data)
				att.set_value_date_quality( data,
				value_.timestamp.tv_sec,
				value_.quality );
		}

		virtual void write(Tango::DeviceImpl *dev, Tango::WAttribute &att)
		{
			T write_value;
			att.get_write_value( write_value );
			boost::any write_value_any = write_value;
			cache_->on_write( this->name, distant_attr_info_.name, write_value_any );
		}

		virtual bool is_allowed (Tango::DeviceImpl *dev, Tango::AttReqType ty)
		{
			return true;
		}

	public: //- 

		virtual Tango::Attr* get_tango_interface()
		{
			return this;
		}

		virtual void on_read_attr_hardware()
		{
			// update value from cache
			value_ = cache_->get_cached_data( this->name );
		}

	private:
		Value  value_;
		CacheP cache_;
		Tango::AttributeInfo distant_attr_info_;
	};


	class StringScalarAttr : public Tango::Attr,
		public ProxyAttr
	{
	public: //- [typedefs]

	public: //- [structors]
		StringScalarAttr( CacheP cache,
			const std::string& name,
			const Tango::AttributeInfo& info )
			: Tango::Attr( name.c_str(), Tango::DEV_STRING, info.disp_level, info.writable ),
			cache_(cache),
			distant_attr_info_(info),
			str_value_(0)
		{
			Tango::UserDefaultAttrProp	prop;
			prop.set_label        ( info.name.c_str() );
			prop.set_description  ( info.description.c_str() );
			prop.set_unit         ( info.unit.c_str() );
			prop.set_standard_unit( info.standard_unit.c_str() );
			prop.set_display_unit ( info.display_unit.c_str() );
			prop.set_format       ( info.format.c_str() );

			/*
			prop.set_min_value    ( info.min_value.c_str() );
			prop.set_max_value    ( info.max_value.c_str() );

			prop.set_min_alarm    ( info.alarms.min_alarm.c_str() );
			prop.set_max_alarm    ( info.alarms.max_alarm.c_str() );
			prop.set_min_warning  ( info.alarms.min_warning.c_str() );
			prop.set_max_warning  ( info.alarms.max_warning.c_str() );
			*/
			this->set_default_properties(prop);
		}

		~StringScalarAttr()
		{
			cache_.reset();
		}

	public: //- [Tango::Attr impl]

		virtual void read(Tango::DeviceImpl *dev, Tango::Attribute &att)
		{
			std::string* data = boost::any_cast<std::string>( &value_.data );
			if (data)
			{
				str_value_ = const_cast<char*>(data->c_str());
				if (str_value_)
					att.set_value_date_quality( &str_value_,
					value_.timestamp.tv_sec,
					value_.quality );
			}
		}

		virtual void write(Tango::DeviceImpl *dev, Tango::WAttribute &att)
		{
			Tango::DevString write_value;
			att.get_write_value( write_value );
			boost::any write_value_any = std::string (write_value);
			cache_->on_write( this->name, distant_attr_info_.name, write_value_any );
		}

		virtual bool is_allowed (Tango::DeviceImpl *dev, Tango::AttReqType ty)
		{
			return true;
		}

	public: //- 

		virtual Tango::Attr* get_tango_interface()
		{
			return this;
		}

		virtual void on_read_attr_hardware()
		{
			// update value from cache
			value_ = cache_->get_cached_data( this->name );
		}

	private:
		Value  value_;
		char* str_value_;

		CacheP cache_;
		Tango::AttributeInfo distant_attr_info_;
	};


	template< typename T >
	class SpectrumAttr : public Tango::SpectrumAttr,
		public ProxyAttr
	{
	public: //- [typedefs]
		typedef typename Types<T>::Buffer  Data;
		typedef typename Types<T>::BufferP DataP;


	public: //- [structors]
		SpectrumAttr( CacheP cache,
			const std::string& name,
			const Tango::AttributeInfo& info )
			: Tango::SpectrumAttr( name.c_str(), TangoTraits<T>::type_id, info.writable, LONG_MAX, info.disp_level ),
			cache_(cache),
			distant_attr_info_(info)
		{
			Tango::UserDefaultAttrProp	prop;
			prop.set_label        ( info.name.c_str() );
			prop.set_description  ( info.description.c_str() );
			prop.set_unit         ( info.unit.c_str() );
			prop.set_standard_unit( info.standard_unit.c_str() );
			prop.set_display_unit ( info.display_unit.c_str() );
			prop.set_format       ( info.format.c_str() );
			/*
			prop.set_min_value    ( info.min_value.c_str() );
			prop.set_max_value    ( info.max_value.c_str() );

			prop.set_min_alarm    ( info.alarms.min_alarm.c_str() );
			prop.set_max_alarm    ( info.alarms.max_alarm.c_str() );
			prop.set_min_warning  ( info.alarms.min_warning.c_str() );
			prop.set_max_warning  ( info.alarms.max_warning.c_str() );
			*/
			this->set_default_properties(prop);
		}

		~SpectrumAttr()
		{
			cache_.reset();
		}

	public: //- [Tango::Attr impl]

		virtual void read(Tango::DeviceImpl *dev, Tango::Attribute &att)
		{
			DataP* data = boost::any_cast<DataP>( &value_.data );
			if (data)
			{
				DataP data_ptr = *data;
				if (data_ptr)
				{
					att.set_value_date_quality( data_ptr->data(),
						value_.timestamp.tv_sec,
						value_.quality,
						data_ptr->shape()[0] );
				}
			}
		}

		virtual void write(Tango::DeviceImpl *dev, Tango::WAttribute &att)
		{
			const T* data;
			att.get_write_value( data );
			long length = att.get_w_dim_x();

			DataP buffer( new Data( boost::extents[length] ) );
			std::copy( data, data + length, buffer->data() );
			cache_->on_write( this->name, distant_attr_info_.name, buffer );
		}

		virtual bool is_allowed (Tango::DeviceImpl *dev, Tango::AttReqType ty)
		{
			return true;
		}


	public: //- 

		virtual Tango::Attr* get_tango_interface()
		{
			return this;
		}

		virtual void on_read_attr_hardware()
		{
			// update value from cache
			value_ = cache_->get_cached_data( this->name );
		}

	private:
		Value  value_;
		CacheP cache_;
		Tango::AttributeInfo distant_attr_info_;
	};

	class StringSpectrumAttr : public Tango::SpectrumAttr,
		public ProxyAttr
	{
	public: //- [typedefs]
		typedef boost::multi_array<std::string, 1> Data;
		typedef boost::shared_ptr<Data>            DataP;


	public: //- [structors]
		StringSpectrumAttr( CacheP cache,
			const std::string& name,
			const Tango::AttributeInfo& info )
			: Tango::SpectrumAttr( name.c_str(), Tango::DEV_STRING, info.writable, LONG_MAX, info.disp_level ),
			cache_(cache),
			distant_attr_info_(info)
		{
			Tango::UserDefaultAttrProp	prop;
			prop.set_label        ( info.name.c_str() );
			prop.set_description  ( info.description.c_str() );
			prop.set_unit         ( info.unit.c_str() );
			prop.set_standard_unit( info.standard_unit.c_str() );
			prop.set_display_unit ( info.display_unit.c_str() );
			prop.set_format       ( info.format.c_str() );
			/*
			prop.set_min_value    ( info.min_value.c_str() );
			prop.set_max_value    ( info.max_value.c_str() );

			prop.set_min_alarm    ( info.alarms.min_alarm.c_str() );
			prop.set_max_alarm    ( info.alarms.max_alarm.c_str() );
			prop.set_min_warning  ( info.alarms.min_warning.c_str() );
			prop.set_max_warning  ( info.alarms.max_warning.c_str() );
			*/
			this->set_default_properties(prop);
		}

		~StringSpectrumAttr()
		{
			cache_.reset();
		}

	public: //- [Tango::Attr impl]

		virtual void read(Tango::DeviceImpl *dev, Tango::Attribute &att)
		{
			DataP* data = boost::any_cast<DataP>( &value_.data );
			if (data)
			{
				DataP data_ptr = *data;
				if (data_ptr)
				{
					std::string* string_data = data_ptr->data();
					str_value_.resize(data_ptr->num_elements());
					for (size_t i = 0; i < data_ptr->num_elements(); i++)
					{
						str_value_[i] = const_cast<Tango::DevString>(string_data[i].c_str());
					}

					att.set_value_date_quality( &str_value_.front(),
						value_.timestamp.tv_sec,
						value_.quality,
						data_ptr->num_elements() );
				}
			}
		}

		virtual void write(Tango::DeviceImpl *dev, Tango::WAttribute &att)
		{
			const Tango::ConstDevString* data;
			att.get_write_value( data );
			long length = att.get_w_dim_x();

			DataP buffer( new Data( boost::extents[length] ) );
			std::copy( data, data + length, buffer->data() );
			cache_->on_write( this->name, distant_attr_info_.name, buffer );
		}

		virtual bool is_allowed (Tango::DeviceImpl *dev, Tango::AttReqType ty)
		{
			return true;
		}


	public: //- 

		virtual Tango::Attr* get_tango_interface()
		{
			return this;
		}

		virtual void on_read_attr_hardware()
		{
			// update value from cache
			value_ = cache_->get_cached_data( this->name );
		}

	private:
		Value  value_;
		std::vector< Tango::DevString > str_value_;
		CacheP cache_;
		Tango::AttributeInfo distant_attr_info_;
	};

	template< typename T >
	class ImageAttr : public Tango::ImageAttr,
		public ProxyAttr
	{
	public: //- [typedefs]
		typedef typename Types<T>::Image  Data;
		typedef typename Types<T>::ImageP DataP;


	public: //- [structors]
		ImageAttr( CacheP cache,
			const std::string& name,
			const Tango::AttributeInfo& info )
			: Tango::ImageAttr( name.c_str(), TangoTraits<T>::type_id, info.writable, LONG_MAX, LONG_MAX, info.disp_level ),
			cache_(cache),
			distant_attr_info_(info)
		{
			Tango::UserDefaultAttrProp	prop;
			prop.set_label        ( info.name.c_str() );
			prop.set_description  ( info.description.c_str() );
			prop.set_unit         ( info.unit.c_str() );
			prop.set_standard_unit( info.standard_unit.c_str() );
			prop.set_display_unit ( info.display_unit.c_str() );
			prop.set_format       ( info.format.c_str() );

			/*
			prop.set_min_value    ( info.min_value.c_str() );
			prop.set_max_value    ( info.max_value.c_str() );

			prop.set_min_alarm    ( info.alarms.min_alarm.c_str() );
			prop.set_max_alarm    ( info.alarms.max_alarm.c_str() );
			prop.set_min_warning  ( info.alarms.min_warning.c_str() );
			prop.set_max_warning  ( info.alarms.max_warning.c_str() );
			*/
			this->set_default_properties(prop);
		}

		~ImageAttr()
		{
			cache_.reset();
		}

	public: //- [Tango::Attr impl]

		virtual void read(Tango::DeviceImpl *dev, Tango::Attribute &att)
		{
			DataP* data = boost::any_cast<DataP>( &value_.data );
			if (data)
			{
				DataP data_ptr = *data;
				if (data_ptr)
				{
					att.set_value_date_quality( data_ptr->data(),
						value_.timestamp.tv_sec,
						value_.quality,
						data_ptr->shape()[1],
						data_ptr->shape()[0] );
				}
			}
		}

		virtual void write(Tango::DeviceImpl *dev, Tango::WAttribute &att)
		{
			const T* data;
			att.get_write_value( data );
			long length_x = att.get_w_dim_x();
			long length_y = att.get_w_dim_y();

			DataP image( new Data( boost::extents[length_y][length_x] ) );
			std::copy( data, data + length_x * length_y, image->data() );
			cache_->on_write( this->name, distant_attr_info_.name, image );
		}

		virtual bool is_allowed (Tango::DeviceImpl *dev, Tango::AttReqType ty)
		{
			return true;
		}


	public: //- 

		virtual Tango::Attr* get_tango_interface()
		{
			return this;
		}

		virtual void on_read_attr_hardware()
		{
			// update value from cache
			value_ = cache_->get_cached_data( this->name );
		}

	private:
		Value  value_;
		CacheP cache_;
		Tango::AttributeInfo distant_attr_info_;
	};

	class StringImageAttr : public Tango::ImageAttr,
		public ProxyAttr
	{
	public: //- [typedefs]
		typedef boost::multi_array<std::string, 2> Data;
		typedef boost::shared_ptr<Data>            DataP;

	public: //- [structors]
		StringImageAttr( CacheP cache,
			const std::string& name,
			const Tango::AttributeInfo& info )
			: Tango::ImageAttr( name.c_str(), Tango::DEV_STRING, info.writable, LONG_MAX, LONG_MAX, info.disp_level ),
			cache_(cache),
			distant_attr_info_(info)
		{
			Tango::UserDefaultAttrProp	prop;
			prop.set_label        ( info.name.c_str() );
			prop.set_description  ( info.description.c_str() );
			prop.set_unit         ( info.unit.c_str() );
			prop.set_standard_unit( info.standard_unit.c_str() );
			prop.set_display_unit ( info.display_unit.c_str() );
			prop.set_format       ( info.format.c_str() );
			/*
			prop.set_min_value    ( info.min_value.c_str() );
			prop.set_max_value    ( info.max_value.c_str() );

			prop.set_min_alarm    ( info.alarms.min_alarm.c_str() );
			prop.set_max_alarm    ( info.alarms.max_alarm.c_str() );
			prop.set_min_warning  ( info.alarms.min_warning.c_str() );
			prop.set_max_warning  ( info.alarms.max_warning.c_str() );
			*/
			this->set_default_properties(prop);
		}

		~StringImageAttr()
		{
			cache_.reset();
		}

	public: //- [Tango::Attr impl]

		virtual void read(Tango::DeviceImpl *dev, Tango::Attribute &att)
		{
			DataP* data = boost::any_cast<DataP>( &value_.data );
			if (data)
			{
				DataP data_ptr = *data;
				if (data_ptr)
				{
					std::string* string_data = data_ptr->data();
					str_value_.resize(data_ptr->num_elements());
					for (size_t i = 0; i < data_ptr->num_elements(); i++)
					{
						str_value_[i] = const_cast<Tango::DevString>(string_data[i].c_str());
					}

					att.set_value_date_quality( &str_value_.front(),
						value_.timestamp.tv_sec,
						value_.quality,
						data_ptr->shape()[1],
						data_ptr->shape()[0] );
				}
			}
		}

		virtual void write(Tango::DeviceImpl *dev, Tango::WAttribute &att)
		{
			const Tango::ConstDevString* data;
			att.get_write_value( data );
			long length_x = att.get_w_dim_x();
			long length_y = att.get_w_dim_y();

			DataP image( new Data( boost::extents[length_y][length_x] ) );
			std::copy( data, data + length_x * length_y, image->data() );
			cache_->on_write( this->name, distant_attr_info_.name, image );

		}

		virtual bool is_allowed (Tango::DeviceImpl *dev, Tango::AttReqType ty)
		{
			return true;
		}


	public: //- 

		virtual Tango::Attr* get_tango_interface()
		{
			return this;
		}

		virtual void on_read_attr_hardware()
		{
			// update value from cache
			value_ = cache_->get_cached_data( this->name );
		}

	private:
		Value  value_;
		std::vector< Tango::DevString > str_value_;
		CacheP cache_;
		Tango::AttributeInfo distant_attr_info_;
	};

	ProxyAttrP ProxyInterfaceFactory::create_attr( const std::string& attr_name,
		const Tango::AttributeInfo& attr_info,
		CacheP cache )
	{

		ProxyAttr* attr = 0;

		switch ( attr_info.data_format )
		{
		case Tango::SCALAR:
			{
				switch ( attr_info.data_type )
				{
				case Tango::DEV_SHORT:   attr = new ScalarAttr<Tango::DevShort>  (cache, attr_name, attr_info); break;
				case Tango::DEV_LONG:    attr = new ScalarAttr<Tango::DevLong>   (cache, attr_name, attr_info); break;
				case Tango::DEV_DOUBLE:  attr = new ScalarAttr<Tango::DevDouble> (cache, attr_name, attr_info); break;
				case Tango::DEV_FLOAT:   attr = new ScalarAttr<Tango::DevFloat>  (cache, attr_name, attr_info); break;
				case Tango::DEV_BOOLEAN: attr = new ScalarAttr<Tango::DevBoolean>(cache, attr_name, attr_info); break;
				case Tango::DEV_USHORT:  attr = new ScalarAttr<Tango::DevUShort> (cache, attr_name, attr_info); break;
				case Tango::DEV_ULONG:   attr = new ScalarAttr<Tango::DevULong>  (cache, attr_name, attr_info); break;
				case Tango::DEV_UCHAR:   attr = new ScalarAttr<Tango::DevUChar>  (cache, attr_name, attr_info); break;
				case Tango::DEV_LONG64:  attr = new ScalarAttr<Tango::DevLong64> (cache, attr_name, attr_info); break;
				case Tango::DEV_ULONG64: attr = new ScalarAttr<Tango::DevULong64>(cache, attr_name, attr_info); break;
				case Tango::DEV_STRING:  attr = new StringScalarAttr             (cache, attr_name, attr_info); break;
				}
			}
			break;
		case Tango::SPECTRUM:
			{
				switch ( attr_info.data_type )
				{
				case Tango::DEV_SHORT:   attr = new SpectrumAttr<Tango::DevShort>  (cache, attr_name, attr_info); break;
				case Tango::DEV_LONG:    attr = new SpectrumAttr<Tango::DevLong>   (cache, attr_name, attr_info); break;
				case Tango::DEV_DOUBLE:  attr = new SpectrumAttr<Tango::DevDouble> (cache, attr_name, attr_info); break;
				case Tango::DEV_FLOAT:   attr = new SpectrumAttr<Tango::DevFloat>  (cache, attr_name, attr_info); break;
				case Tango::DEV_BOOLEAN: attr = new SpectrumAttr<Tango::DevBoolean>(cache, attr_name, attr_info); break;
				case Tango::DEV_USHORT:  attr = new SpectrumAttr<Tango::DevUShort> (cache, attr_name, attr_info); break;
				case Tango::DEV_ULONG:   attr = new SpectrumAttr<Tango::DevULong>  (cache, attr_name, attr_info); break;
				case Tango::DEV_UCHAR:   attr = new SpectrumAttr<Tango::DevUChar>  (cache, attr_name, attr_info); break;
				case Tango::DEV_LONG64:  attr = new SpectrumAttr<Tango::DevLong64> (cache, attr_name, attr_info); break;
				case Tango::DEV_ULONG64: attr = new SpectrumAttr<Tango::DevULong64>(cache, attr_name, attr_info); break;
				case Tango::DEV_STRING:  attr = new StringSpectrumAttr             (cache, attr_name, attr_info); break;
				}
			}
			break;
		case Tango::IMAGE:
			{
				switch ( attr_info.data_type )
				{
				case Tango::DEV_SHORT:   attr = new ImageAttr<Tango::DevShort>  (cache, attr_name, attr_info); break;
				case Tango::DEV_LONG:    attr = new ImageAttr<Tango::DevLong>   (cache, attr_name, attr_info); break;
				case Tango::DEV_DOUBLE:  attr = new ImageAttr<Tango::DevDouble> (cache, attr_name, attr_info); break;
				case Tango::DEV_FLOAT:   attr = new ImageAttr<Tango::DevFloat>  (cache, attr_name, attr_info); break;
				case Tango::DEV_BOOLEAN: attr = new ImageAttr<Tango::DevBoolean>(cache, attr_name, attr_info); break;
				case Tango::DEV_USHORT:  attr = new ImageAttr<Tango::DevUShort> (cache, attr_name, attr_info); break;
				case Tango::DEV_ULONG:   attr = new ImageAttr<Tango::DevULong>  (cache, attr_name, attr_info); break;
				case Tango::DEV_UCHAR:   attr = new ImageAttr<Tango::DevUChar>  (cache, attr_name, attr_info); break;
				case Tango::DEV_LONG64:  attr = new ImageAttr<Tango::DevLong64> (cache, attr_name, attr_info); break;
				case Tango::DEV_ULONG64: attr = new ImageAttr<Tango::DevULong64>(cache, attr_name, attr_info); break;
				case Tango::DEV_STRING:  attr = new StringImageAttr             (cache, attr_name, attr_info); break;
				}
			}
			break;
		}

		return ProxyAttrP(attr);
	}


	class Command : public Tango::Command,public ProxyCmd,public Tango::LogAdapter
	{
	public: //- [structors]

		Command( const std::string& cmd_name,
			const Tango::CommandInfo& cmd_info,
			DeviceProxyP device_proxy, 
			Tango::Device_3Impl* device)
			: Tango::Command( cmd_name.c_str(),
			static_cast<Tango::CmdArgType>(cmd_info.in_type),
			static_cast<Tango::CmdArgType>(cmd_info.out_type),
			cmd_info.in_type_desc.c_str(),
			cmd_info.out_type_desc.c_str(),
			cmd_info.disp_level),
			Tango::LogAdapter(device),
			cmd_info_(cmd_info),
			cmd_name_(cmd_name),
			device_proxy_(device_proxy)
		{
		}

	public: //- [Tango::Command impl]

		virtual CORBA::Any *execute (Tango::DeviceImpl * my_proxy, const CORBA::Any &in_any)
		{
			// first get the state of the read_only attribute
			Proxy_ns::Proxy * p = 0;
			try 
			{
				INFO_STREAM << "Local command "+ cmd_name_+" sended to distant device."<< ENDLOG;
				p = reinterpret_cast<Proxy_ns::Proxy*> (my_proxy);
			}
			catch (...)
			{
				throw_devfailed("EXECUTION_ERROR",
					"reinterpret_cast<Proxy_ns::Proxy*>  failed",
					"Command::execute");
			}

			if (p->read_only())	
				throw_devfailed("EXECUTION_ERROR",
				"Command execution impossible because Proxy is read_only",
				"Command::execute");

			// read_only is OFF so command maybe executed

			CORBA::Any_var out_data = device_proxy_->command_inout( cmd_info_.cmd_name, const_cast<CORBA::Any&>(in_any) );
			return out_data._retn();
		}

	public: //- [ProxyCmd impl]

		virtual Tango::Command* get_tango_interface()
		{
			return this;
		}

	private:
		Tango::CommandInfo cmd_info_;
		std::string cmd_name_;
		DeviceProxyP device_proxy_;
	};


	ProxyCmdP ProxyInterfaceFactory::create_cmd( const std::string& cmd_name,
		const Tango::CommandInfo& cmd_info,
		DeviceProxyP proxy, 
		Tango::Device_3Impl* device)
	{
		ProxyCmdP cmd( new Command(cmd_name, cmd_info, proxy, device) );
		return cmd;
	}

}
