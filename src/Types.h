#ifndef _TYPES_H_
#define _TYPES_H_

#include <boost/smart_ptr.hpp>
#include <boost/multi_array.hpp>
#include <tango.h>

namespace Proxy_ns
{

  template <typename T>
  struct Types
  {
    typedef boost::multi_array<T, 1>  Buffer;
    typedef boost::multi_array<T, 2>  Image;

    typedef boost::shared_ptr<Buffer> BufferP;
    typedef boost::shared_ptr<Image>  ImageP;
  };

  template <typename T>
  struct TangoTraits
  {
  };

# define DEFINE_TANGO_TRAITS( Type, TypeID, DevVarArrayType, DeviceAttributeMember ) \
    template<> struct TangoTraits<Type> \
    { \
      BOOST_STATIC_CONSTANT( int, type_id = TypeID ); \
      \
      typedef DevVarArrayType DevVarArray_var; \
      static DevVarArray_var& internal_buffer( Tango::DeviceAttribute& dev_attr ) \
      { \
        return dev_attr.DeviceAttributeMember; \
      } \
    }

  DEFINE_TANGO_TRAITS( Tango::DevBoolean, Tango::DEV_BOOLEAN, Tango::DevVarBooleanArray_var, BooleanSeq );

  DEFINE_TANGO_TRAITS( Tango::DevUChar,   Tango::DEV_UCHAR,   Tango::DevVarCharArray_var,    UCharSeq );

  DEFINE_TANGO_TRAITS( Tango::DevUShort,  Tango::DEV_USHORT,  Tango::DevVarUShortArray_var,  UShortSeq );
  DEFINE_TANGO_TRAITS( Tango::DevShort,   Tango::DEV_SHORT,   Tango::DevVarShortArray_var,   ShortSeq );

  DEFINE_TANGO_TRAITS( Tango::DevLong,    Tango::DEV_LONG,    Tango::DevVarLongArray_var,    LongSeq );
#if (TANGO_VERSION_MAJOR >= 9)
#pragma message "TANGO_VERSION_MAJOR >= 9"
  DEFINE_TANGO_TRAITS( Tango::DevULong,   Tango::DEV_ULONG,   Tango::DevVarULongArray_var,   ULongSeq );
  DEFINE_TANGO_TRAITS( Tango::DevULong64, Tango::DEV_ULONG64, Tango::DevVarULong64Array_var, ULong64Seq );
  DEFINE_TANGO_TRAITS( Tango::DevLong64,  Tango::DEV_LONG64,  Tango::DevVarLong64Array_var,  Long64Seq );
#else
  DEFINE_TANGO_TRAITS( Tango::DevULong,   Tango::DEV_ULONG,   Tango::DevVarULongArray_var,   get_ULong_data() );
  DEFINE_TANGO_TRAITS( Tango::DevULong64, Tango::DEV_ULONG64, Tango::DevVarULong64Array_var, get_ULong64_data() );
  DEFINE_TANGO_TRAITS( Tango::DevLong64,  Tango::DEV_LONG64,  Tango::DevVarLong64Array_var,  get_Long64_data() );
#endif

  DEFINE_TANGO_TRAITS( Tango::DevDouble,  Tango::DEV_DOUBLE,  Tango::DevVarDoubleArray_var,  DoubleSeq );
  DEFINE_TANGO_TRAITS( Tango::DevFloat,   Tango::DEV_FLOAT,   Tango::DevVarFloatArray_var,   FloatSeq );
}

#endif
